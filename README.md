## But du projet

Développer en C un serveur de jeu zappy et un client graphique en C ou Perl.

## Principe

Le jeu consiste à gérer un univers et ses habitants. Ce monde, nommé Trantor, est géographiquement constitué de plaines et ne comprend aucun autre relief.

Le plateau de jeu représente la totalité de la surface de ce monde, comme un planisphère. Des ressources alimentaires et minières sont générées aléatoirement sur le terrain.

Le Trantorien est pacifique. Il n’est ni violent, ni agressif. Il déambule gaiement en quête de pierres et s’alimente au passage. Il croise sans difficulté ses semblables sur une même unité de terrain et voit aussi loin que ses capacités visuelles le lui permettent.

C’est un être immatériel, flou, qui occupe toute la case dans laquelle il se trouve. Il est impossible de distinguer son orientation lorsqu’on le croise. La nourriture qu’il ramasse lui permet de vivre pendant un certain laps de temps.

L’objectif des joueurs est de s’élever dans la hiérarchie trantorienne. Cette "augmentation", qui permet d’accroître ses capacités physiques et mentales, doit être effectuée selon un rite particulier. Il faut en effet réunir sur la même unité de terrain:

Une combinaison de pierres,
Un certain nombre de joueurs de même niveau.
Un joueur débute l’incantation et lance ainsi le processus d’élévation. Il n’est pas nécessaire que les participants soient de la même équipe. Seul leur niveau importe. Tous les joueurs du groupe réalisant l’incantation atteignent le niveau supérieur.

Pour compliquer la tâche de l’IA, le champ de vision des joueurs est limité. A chaque élévation, la vision augmente d’une unité de mesure en avant et d’une de chaque côté de la nouvelle rangée.

Un Trantorien a également moyen de détecter la présence de ses congénères en émettant une onde-radar lui indiquant la direction à prendre pour les rejoindre.

Le jeu se joue par équipe. L’équipe gagnante est celle dont six joueurs auront atteint l’élévation maximale.